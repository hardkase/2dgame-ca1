This is the outline of to-dos for my game, tentatively named "Fire for Effect!"
For 2D Game Engine Development
CA 1
Patrick Collins GD 1
Sole Proprietor

game skeleton

create player
- dimensions
- starting location
- range (if l/r motion)
- image/sprite
create enemy
- dimensions
- starting location
- range (if l/r motion)
- image/sprite
create bullet
- dimensions
- image
- animation (spinning ball)
create fire method including trajectory drawing
- x/y 
- +/- wind
create turn based logic
- gamestate
- levelstate
create hit/miss logic
- animate destruction
create enemy move/fire logic
-simple AI
determine win/lose condition
-1 hit or multiple hits
create levels/level logic
-limited bullets
- wind + or -minus
- enemy elevated, better terrain protection
- difficult terrain
- enemy health regen
create terrain
- flat, middle hill, cliffside enemy, elevated player, elevated enemy etc
auto-generate terrain
- tricky; trickier still to bake in levelling requirements
use sprites/images
animate where possible
-research needed (think multiple sprites do da trick)
create scoring logic
- default win score minus hits minus num rounds fired or time elapsed
store scores local
store scores remote
2 column db -DoR's ajax stuff